autoload -Uz compinit
compinit

autoload -Uz colors
colors

# URL のペースト時にエスケープする
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

PROMPT=' %(?.%F{cyan} .%F{red} )%f%F{cyan}%d%f ${vcs_info_msg_0_}
%(!. %F{white}%B#%b%f.)  '
RPROMPT="%(?..%F{red}%?%f)"
SPROMPT="zsh: %F{red}%B%R%b%f を %F{green}%B%r%b%f に修正しますか？ [y]es [n]o [a]bort [e]dit :"
HISTFILE=~/.zhistory
HISTSIZE=10000
SAVEHIST=10000
setopt share_history
setopt extended_history
setopt hist_ignore_dups
setopt histignorespace #コマンドの最初にスペースが入っていたら保存しない
setopt interactive_comments
setopt notify
setopt hist_ignore_all_dups
setopt auto_cd
setopt correct
#setopt correct_all
REPORTTIME=5
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' list-dirs-first
zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*:approximate:*' max-errors 3 numeric
zstyle ':completion:*' ignored-patterns '_*'
zstyle ':completion:*' use-cache true
zstyle ':completion:*' menu select
alias ll="ls -l"
alias :http="python -m http.server --bind ::"
alias -g ":g"="| grep"
alias -g ":l"="|less"
alias -g "copy"="xclip -selection c"
chpwd() { ls --color=auto -F}

setopt PROMPT_SUBST
TRAPALRM(){
	if [ "$WIDGET" != "expand-or-complete" ]; then
		vcs_info
		zle reset-prompt
	fi
}
TMOUT=30
autoload -Uz vcs_info
setopt prompt_subst
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' stagedstr "%F{green}+"
zstyle ':vcs_info:git:*' unstagedstr "%F{yellow}*"
zstyle ':vcs_info:*' formats "%F{blue} %b%c%u%f "
zstyle ':vcs_info:*' actionformats ':%a (%b)'
precmd () { vcs_info }
bindkey -e
bindkey ";5D" backward-word
bindkey ";5C" forward-word
bindkey ";3D" backward-word
bindkey ";3C" forward-word

# Syntax Highlighiting
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern regexp)

source /usr/share/doc/pkgfile/command-not-found.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
ZSH_HIGHLIGHT_REGEXP+=('\bsudo\b' bold)


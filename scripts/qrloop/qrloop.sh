#!/bin/bash
mkdir -p tmp
cd tmp
echo $@|qrencode -o 1.png
convert -trim -define webp:lossless=true 1.png 1.webp
rm 1.png
i=0
while true
do
i=$(expr $i + 1)
if [ $(wc -c $i.webp|awk '{print $1}') -lt 2954 ];then
cat $i.webp|qrencode -8 -o $(expr $i + 1).png
convert -trim -define webp:lossless=true $(expr $i + 1).png $(expr $i + 1).webp
rm $(expr $i + 1).png
else
convert $i.webp -bordercolor "#fff" -border 4x4 ../qr.png
break
fi
done
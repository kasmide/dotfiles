#!/bin/zsh
mkdir -p tmp
i=0
MIME=$(zbarimg --quiet --raw $@|iconv -f UTF-8 -t ISO-8859-1|file - --mime-type|awk '{print $2}')
zbarimg --quiet --raw $@ | iconv -f UTF-8 -t ISO-8859-1>tmp/$(expr $i + 1).$(cat /etc/mime.types|awk "\$1==\"$MIME\"{print \$2}"|head -n1)
cd tmp
while true
do
if [[ ! $MIME =~ "image/.*" ]];then
i=$(expr $i + 1)
mv $(ls|grep -E "^$i\..+"|head -n1) ../result.$(cat /etc/mime.types|awk "\$1==\"$MIME\"{print \$2}"|head -n1)
break
fi
i=$(expr $i + 1)
MIME=$(zbarimg --quiet --raw $(ls|grep -E "^$i\..+"|head -n1)|iconv -f UTF-8 -t ISO-8859-1|file - --mime-type|awk '{print $2}')
zbarimg --quiet --raw $(ls|grep -E "^$i\..+"|head -n1) | iconv -f UTF-8 -t ISO-8859-1>$(expr $i + 1).$(cat /etc/mime.types|awk "\$1==\"$MIME\"{print \$2}"|head -n1)
done
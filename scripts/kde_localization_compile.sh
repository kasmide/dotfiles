# This compiles all the .po file you modified and put them to locale folder
# Place this file on your copied KDE l10n svn repository
LANGCODE=ja # Modify this variable to the language code you want to use
mkdir -p $HOME/.local/share/locale/${LANGCODE}/LC_MESSAGES/
for i in $(svn status|grep -e ^M|sed s/"M     "//g);
do
    if [[ $i =~ .*_qt\.po$ ]];then
        lconvert $i -o $HOME/.local/share/locale/${LANGCODE}/LC_MESSAGES/$(basename -s ".po" $i).qm
    else
        msgfmt $i -o $HOME/.local/share/locale/${LANGCODE}/LC_MESSAGES/$(basename -s ".po" $i).mo
    fi
done